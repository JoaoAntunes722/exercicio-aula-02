/* Escrevam uma função em Javascript que retorna um número aleatório entre 1 e 10 depois de 3 segundos. 
A função deve retornar uma Promessa que, caso o número aleatório seja menor que 5, retorna sucesso. 
Se o número for maior que 5, retorna erro.
*/

function getRandomInt(max) {
    return Math.floor(Math.random() * max + 1);
  }

function resolveAfter3Seconds() {
    return new Promise((resolve, reject) => {
      setTimeout(() => { 
        const randomNumber = getRandomInt(10); // Gera um número aleatório entre 1 e 10.
        if (randomNumber < 5) {
            resolve(randomNumber); // Resolve com sucesso se o número for menor que 5.
        } else {
            reject(new Error('Erro: número maior ou igual a 5')); // Rejeita com erro se o número for maior ou igual a 5.
        }
      }, 3000);
    });
  }
  
  async function asyncCall() {
    console.log('Gerando numero aleatório');
    try {
        const result = await resolveAfter3Seconds();
        console.log('Número aleatório gerado:', result);
    } catch (error) {
        console.error(error.message);
    }
  }
  
  asyncCall();